The Lore folder will usually contain plain text files describing any particular aspect of the game's lore.
These are all canon and can be used for reference when developing other content.


The Storylines folder will usually contain HTML files to be imported to Twine 2 at https://twinery.org/2/ in order to view and play the storyboard.

Other files relating to the game's narrative will be in the main repository, including level progression and other such design elements.